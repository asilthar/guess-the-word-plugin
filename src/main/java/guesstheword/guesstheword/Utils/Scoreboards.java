package guesstheword.guesstheword.Utils;

import guesstheword.guesstheword.GuessTheWord;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

public class Scoreboards {

    public static void lobbyScoreboard(Player p){
        ScoreboardManager manager = Bukkit.getScoreboardManager();
        Scoreboard sb = manager.getNewScoreboard();
        Objective objective = sb.registerNewObjective("Objective","dummy", ChatColor.LIGHT_PURPLE+""+ChatColor.BOLD+"Guess the word!");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        Score score12 = objective.getScore(ChatColor.AQUA+"Guess what the"+ChatColor.GREEN+""+ChatColor.BOLD+" builder");
        score12.setScore(12);
        Score score11 = objective.getScore(ChatColor.AQUA+"is building!");
        score11.setScore(11);
        Score score10 = objective.getScore(" ");
        score10.setScore(10);
        Score score9 = objective.getScore(ChatColor.AQUA+"Write in"+ChatColor.GREEN+""+ChatColor.BOLD+" chat"+ChatColor.AQUA+" what");
        score9.setScore(9);
        Score score8 = objective.getScore(ChatColor.AQUA+"you think it is!");
        score8.setScore(8);
        Score score7 = objective.getScore("  ");
        score7.setScore(7);
        Score score6 = objective.getScore(ChatColor.GOLD+"Time per round: "+ChatColor.AQUA+ GuessTheWord.getInst().getConfig().getInt("minutes-per-round")+":"+"00");
        score6.setScore(6);
        Score score5 = objective.getScore(ChatColor.GOLD+"Total sets: "+ChatColor.AQUA+GameRound.total_sets);
        score5.setScore(5);
        Score score4 = objective.getScore("   ");
        score4.setScore(4);
        Score score3 = objective.getScore(ChatColor.GREEN+"/guesstheword start " +ChatColor.AQUA+"to");
        score3.setScore(3);
        Score score2 = objective.getScore(ChatColor.AQUA+"start the game!");
        score2.setScore(2);
//        Score score1 = objective.getScore(" ");
//        score1.setScore(1);

        p.setScoreboard(sb);

    }

    public static void clearScoreboard(Player p){
        ScoreboardManager manager = Bukkit.getScoreboardManager();
        Scoreboard sb = manager.getNewScoreboard();
        p.setScoreboard(sb);

    }





}
