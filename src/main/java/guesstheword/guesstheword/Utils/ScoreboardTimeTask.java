package guesstheword.guesstheword.Utils;

import guesstheword.guesstheword.GuessTheWord;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.*;

import java.security.PublicKey;
import java.util.UUID;

public class ScoreboardTimeTask extends BukkitRunnable {

    GuessTheWord plugin;
    public static Integer counter;

    public ScoreboardTimeTask(GuessTheWord plugin, Integer counter) {
        this.plugin = plugin;
        this.counter=counter;
    }

    @Override
    public void run() {

        if(counter>0){


            Integer minutes = counter/60;
            Integer seconds = counter-(60*(counter/60));
            ScoreboardManager manager = Bukkit.getScoreboardManager();
            Scoreboard sb = manager.getNewScoreboard();
            Objective objective = sb.registerNewObjective("Objective","dummy", ChatColor.GREEN+""+ChatColor.BOLD+"Guess the word!");
            objective.setDisplaySlot(DisplaySlot.SIDEBAR);
            Score score12 = objective.getScore(ChatColor.AQUA+"Guess what the"+ChatColor.DARK_PURPLE+""+ChatColor.BOLD+" builder");
            score12.setScore(12);
            Score score11 = objective.getScore(ChatColor.AQUA+"is building!");
            score11.setScore(11);
            Score score10 = objective.getScore(" ");
            score10.setScore(10);
            Score score9 = objective.getScore(ChatColor.AQUA+"Write in"+ChatColor.DARK_PURPLE+""+ChatColor.BOLD+" chat"+ChatColor.AQUA+" what");
            score9.setScore(9);
            Score score8 = objective.getScore(ChatColor.AQUA+"you think it is!");
            score8.setScore(8);
            Score score7 = objective.getScore("  ");
            score7.setScore(7);
            if(seconds>9){
                Score score6 = objective.getScore(ChatColor.GOLD+"Time left: "+ChatColor.AQUA+minutes+":"+seconds);
                score6.setScore(6);
            }else{
                Score score6 = objective.getScore(ChatColor.GOLD+"Time left: "+ChatColor.AQUA+minutes+":"+0+seconds);
                score6.setScore(6);
            }
            Score score6 = objective.getScore("   ");
            score6.setScore(6);
            Score score5 = objective.getScore(ChatColor.GOLD+"Current set: "+ChatColor.AQUA+GameRound.current_set+"/"+GameRound.total_sets);
            score5.setScore(5);
            for(UUID uuid:PlayerLib.players){
                Bukkit.getPlayer(uuid).setScoreboard(sb);
            }
            counter--;
        }else{
            this.cancel();
        }

    }
}
