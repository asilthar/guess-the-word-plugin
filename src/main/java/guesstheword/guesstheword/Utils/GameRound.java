package guesstheword.guesstheword.Utils;

import guesstheword.guesstheword.GuessTheWord;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scheduler.BukkitWorker;

import java.util.List;
import java.util.UUID;

public class GameRound {
//    public static Integer total_sets = GuessTheWord.getInst().getConfig().getInt("game-sets");
    public static Integer total_sets = GuessTheWord.getInst().getConfig().getInt("game-sets");
    public static Integer current_set;
    public static Boolean is_in_progress = false;

    public static void reload_total_sets(){
        total_sets = GuessTheWord.getInst().getConfig().getInt("game-sets");
    }

    //method to instantly end the round and start the next one.
    public static void skipRound(){

        //announce that everyone guessed the answer
        for(UUID uuid:PlayerLib.players){
            Bukkit.getPlayer(uuid).sendMessage(ChatColor.GREEN+"Everyone answered!");
        }

        //Get current and pending tasks and close them if there are any
        BukkitScheduler scheduler = GuessTheWord.getInst().getServer().getScheduler();
        List<BukkitWorker> running_tasks = scheduler.getActiveWorkers();
        List<BukkitTask> pending_tasks = scheduler.getPendingTasks();
        if(!running_tasks.isEmpty()){
            for (BukkitWorker task: running_tasks ){
                int id = task.getTaskId();

                scheduler.cancelTask(id);
            }
        }
        if(!pending_tasks.isEmpty()){
            for (BukkitTask task:pending_tasks){
                int id = task.getTaskId();
                scheduler.cancelTask(id);
            }
        }

        //Extract the current builder (also inserts the next one)
        BukkitTask extract = new PlayerOutTask((GuessTheWord) GuessTheWord.getInst(),Bukkit.getPlayer(PlayerLib.players_not_yet_played.get(0))).runTask(GuessTheWord.getInst());
    }




}
