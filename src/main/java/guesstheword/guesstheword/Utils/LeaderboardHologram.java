package guesstheword.guesstheword.Utils;

import guesstheword.guesstheword.GuessTheWord;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class LeaderboardHologram {
    GuessTheWord plugin;

    public LeaderboardHologram(GuessTheWord plugin) {
        this.plugin = plugin;
    }

    private static ArrayList<ArmorStand> all_armorstands = new ArrayList<>();



    public static void spawn(){
        Location leaderboard_location = new Location(Bukkit.getWorld(GuessTheWord.getInst().getConfig().getString("leaderboard-location.world"))
                ,GuessTheWord.getInst().getConfig().getDouble("leaderboard-location.x")
                ,GuessTheWord.getInst().getConfig().getDouble("leaderboard-location.y")
                ,GuessTheWord.getInst().getConfig().getDouble("leaderboard-location.z"));
        if(PlayerLib.players.size()<1){
        }else if (PlayerLib.players.size() == 1){

            ArmorStand hologram = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location, EntityType.ARMOR_STAND);
            hologram.setVisible(false);
            hologram.setCustomNameVisible(true);
            hologram.setCustomName(ChatColor.DARK_RED+"Leaderboard");
            hologram.setGravity(false);
            if(!(all_armorstands.contains(hologram))){
                all_armorstands.add(hologram);
            }

            ArmorStand hologram2 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.5,0), EntityType.ARMOR_STAND);
            hologram2.setVisible(false);
            hologram2.setCustomNameVisible(true);
            hologram2.setGravity(false);
            hologram2.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(0)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(0)));
            if(!(all_armorstands.contains(hologram2))){
                all_armorstands.add(hologram2);
            }

        } else if (PlayerLib.players.size() == 2){

            ArmorStand hologram = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location, EntityType.ARMOR_STAND);
            hologram.setVisible(false);
            hologram.setCustomNameVisible(true);
            hologram.setCustomName(ChatColor.DARK_RED+"Leaderboard");
            hologram.setGravity(false);
            if(!(all_armorstands.contains(hologram))){
                all_armorstands.add(hologram);
            }

            ArmorStand hologram2 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram2.setVisible(false);
            hologram2.setCustomNameVisible(true);
            hologram2.setGravity(false);
            hologram2.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(0)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(0)));
            if(!(all_armorstands.contains(hologram2))){
                all_armorstands.add(hologram2);
            }

            ArmorStand hologram3 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram3.setVisible(false);
            hologram3.setCustomNameVisible(true);
            hologram3.setGravity(false);
            hologram3.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(1)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(1)));
            if(!(all_armorstands.contains(hologram3))){
                all_armorstands.add(hologram3);
            }

        }else if (PlayerLib.players.size() == 3){

            ArmorStand hologram = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location, EntityType.ARMOR_STAND);
            hologram.setVisible(false);
            hologram.setCustomNameVisible(true);
            hologram.setCustomName(ChatColor.DARK_RED+"Leaderboard");
            hologram.setGravity(false);
            if(!(all_armorstands.contains(hologram))){
                all_armorstands.add(hologram);
            }

            ArmorStand hologram2 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.5,0), EntityType.ARMOR_STAND);
            hologram2.setVisible(false);
            hologram2.setCustomNameVisible(true);
            hologram2.setGravity(false);
            hologram2.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(0)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(0)));
            if(!(all_armorstands.contains(hologram2))){
                all_armorstands.add(hologram2);
            }

            ArmorStand hologram3 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram3.setVisible(false);
            hologram3.setCustomNameVisible(true);
            hologram3.setGravity(false);
            hologram3.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(1)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(1)));
            if(!(all_armorstands.contains(hologram3))){
                all_armorstands.add(hologram3);
            }

            ArmorStand hologram4 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram4.setVisible(false);
            hologram4.setCustomNameVisible(true);
            hologram4.setGravity(false);
            hologram4.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(2)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(2)));
            if(!(all_armorstands.contains(hologram4))){
                all_armorstands.add(hologram4);
            }

        }else if (PlayerLib.players.size() == 4){

            ArmorStand hologram = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location, EntityType.ARMOR_STAND);
            hologram.setVisible(false);
            hologram.setCustomNameVisible(true);
            hologram.setCustomName(ChatColor.DARK_RED+"Leaderboard");
            hologram.setGravity(false);
            if(!(all_armorstands.contains(hologram))){
                all_armorstands.add(hologram);
            }

            ArmorStand hologram2 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.5,0), EntityType.ARMOR_STAND);
            hologram2.setVisible(false);
            hologram2.setCustomNameVisible(true);
            hologram2.setGravity(false);
            hologram2.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(0)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(0)));
            if(!(all_armorstands.contains(hologram2))){
                all_armorstands.add(hologram2);
            }

            ArmorStand hologram3 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram3.setVisible(false);
            hologram3.setCustomNameVisible(true);
            hologram3.setGravity(false);
            hologram3.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(1)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(1)));
            if(!(all_armorstands.contains(hologram3))){
                all_armorstands.add(hologram3);
            }

            ArmorStand hologram4 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram4.setVisible(false);
            hologram4.setCustomNameVisible(true);
            hologram4.setGravity(false);
            hologram4.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(2)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(2)));
            if(!(all_armorstands.contains(hologram4))){
                all_armorstands.add(hologram4);
            }

            ArmorStand hologram5 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram5.setVisible(false);
            hologram5.setCustomNameVisible(true);
            hologram5.setGravity(false);
            hologram5.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(3)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(3)));
            if(!(all_armorstands.contains(hologram5))){
                all_armorstands.add(hologram5);
            }

        }else if (PlayerLib.players.size() == 5){

            ArmorStand hologram = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location, EntityType.ARMOR_STAND);
            hologram.setVisible(false);
            hologram.setCustomNameVisible(true);
            hologram.setCustomName(ChatColor.DARK_RED+"Leaderboard");
            hologram.setGravity(false);
            if(!(all_armorstands.contains(hologram))){
                all_armorstands.add(hologram);
            }

            ArmorStand hologram2 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.5,0), EntityType.ARMOR_STAND);
            hologram2.setVisible(false);
            hologram2.setCustomNameVisible(true);
            hologram2.setGravity(false);
            hologram2.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(0)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(0)));
            if(!(all_armorstands.contains(hologram2))){
                all_armorstands.add(hologram2);
            }

            ArmorStand hologram3 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram3.setVisible(false);
            hologram3.setCustomNameVisible(true);
            hologram3.setGravity(false);
            hologram3.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(1)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(1)));
            if(!(all_armorstands.contains(hologram3))){
                all_armorstands.add(hologram3);
            }

            ArmorStand hologram4 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram4.setVisible(false);
            hologram4.setCustomNameVisible(true);
            hologram4.setGravity(false);
            hologram4.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(2)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(2)));
            if(!(all_armorstands.contains(hologram4))){
                all_armorstands.add(hologram4);
            }

            ArmorStand hologram5 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram5.setVisible(false);
            hologram5.setCustomNameVisible(true);
            hologram5.setGravity(false);
            hologram5.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(3)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(3)));
            if(!(all_armorstands.contains(hologram5))){
                all_armorstands.add(hologram5);
            }

            ArmorStand hologram6 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram6.setVisible(false);
            hologram6.setCustomNameVisible(true);
            hologram6.setGravity(false);
            hologram6.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(4)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(4)));
            if(!(all_armorstands.contains(hologram6))){
                all_armorstands.add(hologram6);
            }

        }else if (PlayerLib.players.size() == 5){

            ArmorStand hologram = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location, EntityType.ARMOR_STAND);
            hologram.setVisible(false);
            hologram.setCustomNameVisible(true);
            hologram.setCustomName(ChatColor.DARK_RED+"Leaderboard");
            hologram.setGravity(false);
            if(!(all_armorstands.contains(hologram))){
                all_armorstands.add(hologram);
            }

            ArmorStand hologram2 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.5,0), EntityType.ARMOR_STAND);
            hologram2.setVisible(false);
            hologram2.setCustomNameVisible(true);
            hologram2.setGravity(false);
            hologram2.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(0)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(0)));
            if(!(all_armorstands.contains(hologram2))){
                all_armorstands.add(hologram2);
            }

            ArmorStand hologram3 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram3.setVisible(false);
            hologram3.setCustomNameVisible(true);
            hologram3.setGravity(false);
            hologram3.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(1)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(1)));
            if(!(all_armorstands.contains(hologram3))){
                all_armorstands.add(hologram3);
            }

            ArmorStand hologram4 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram4.setVisible(false);
            hologram4.setCustomNameVisible(true);
            hologram4.setGravity(false);
            hologram4.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(2)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(2)));
            if(!(all_armorstands.contains(hologram4))){
                all_armorstands.add(hologram4);
            }

            ArmorStand hologram5 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram5.setVisible(false);
            hologram5.setCustomNameVisible(true);
            hologram5.setGravity(false);
            hologram5.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(3)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(3)));
            if(!(all_armorstands.contains(hologram5))){
                all_armorstands.add(hologram5);
            }

            ArmorStand hologram6 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram6.setVisible(false);
            hologram6.setCustomNameVisible(true);
            hologram6.setGravity(false);
            hologram6.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(4)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(4)));
            if(!(all_armorstands.contains(hologram6))){
                all_armorstands.add(hologram6);
            }

        }else if (PlayerLib.players.size() == 6){

            ArmorStand hologram = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location, EntityType.ARMOR_STAND);
            hologram.setVisible(false);
            hologram.setCustomNameVisible(true);
            hologram.setCustomName(ChatColor.DARK_RED+"Leaderboard");
            hologram.setGravity(false);
            if(!(all_armorstands.contains(hologram))){
                all_armorstands.add(hologram);
            }

            ArmorStand hologram2 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.5,0), EntityType.ARMOR_STAND);
            hologram2.setVisible(false);
            hologram2.setCustomNameVisible(true);
            hologram2.setGravity(false);
            hologram2.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(0)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(0)));
            if(!(all_armorstands.contains(hologram2))){
                all_armorstands.add(hologram2);
            }

            ArmorStand hologram3 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram3.setVisible(false);
            hologram3.setCustomNameVisible(true);
            hologram3.setGravity(false);
            hologram3.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(1)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(1)));
            if(!(all_armorstands.contains(hologram3))){
                all_armorstands.add(hologram3);
            }

            ArmorStand hologram4 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram4.setVisible(false);
            hologram4.setCustomNameVisible(true);
            hologram4.setGravity(false);
            hologram4.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(2)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(2)));
            if(!(all_armorstands.contains(hologram4))){
                all_armorstands.add(hologram4);
            }

            ArmorStand hologram5 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram5.setVisible(false);
            hologram5.setCustomNameVisible(true);
            hologram5.setGravity(false);
            hologram5.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(3)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(3)));
            if(!(all_armorstands.contains(hologram5))){
                all_armorstands.add(hologram5);
            }

            ArmorStand hologram6 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram6.setVisible(false);
            hologram6.setCustomNameVisible(true);
            hologram6.setGravity(false);
            hologram6.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(4)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(4)));
            if(!(all_armorstands.contains(hologram6))){
                all_armorstands.add(hologram6);
            }

            ArmorStand hologram7 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram7.setVisible(false);
            hologram7.setCustomNameVisible(true);
            hologram7.setGravity(false);
            hologram7.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(5)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(5)));
            if(!(all_armorstands.contains(hologram7))){
                all_armorstands.add(hologram7);
            }
        }else if (PlayerLib.players.size() == 7){

            ArmorStand hologram = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location, EntityType.ARMOR_STAND);
            hologram.setVisible(false);
            hologram.setCustomNameVisible(true);
            hologram.setCustomName(ChatColor.DARK_RED+"Leaderboard");
            hologram.setGravity(false);
            if(!(all_armorstands.contains(hologram))){
                all_armorstands.add(hologram);
            }

            ArmorStand hologram2 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.5,0), EntityType.ARMOR_STAND);
            hologram2.setVisible(false);
            hologram2.setCustomNameVisible(true);
            hologram2.setGravity(false);
            hologram2.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(0)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(0)));
            if(!(all_armorstands.contains(hologram2))){
                all_armorstands.add(hologram2);
            }

            ArmorStand hologram3 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram3.setVisible(false);
            hologram3.setCustomNameVisible(true);
            hologram3.setGravity(false);
            hologram3.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(1)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(1)));
            if(!(all_armorstands.contains(hologram3))){
                all_armorstands.add(hologram3);
            }

            ArmorStand hologram4 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram4.setVisible(false);
            hologram4.setCustomNameVisible(true);
            hologram4.setGravity(false);
            hologram4.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(2)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(2)));
            if(!(all_armorstands.contains(hologram4))){
                all_armorstands.add(hologram4);
            }

            ArmorStand hologram5 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram5.setVisible(false);
            hologram5.setCustomNameVisible(true);
            hologram5.setGravity(false);
            hologram5.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(3)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(3)));
            if(!(all_armorstands.contains(hologram5))){
                all_armorstands.add(hologram5);
            }

            ArmorStand hologram6 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram6.setVisible(false);
            hologram6.setCustomNameVisible(true);
            hologram6.setGravity(false);
            hologram6.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(4)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(4)));
            if(!(all_armorstands.contains(hologram6))){
                all_armorstands.add(hologram6);
            }

            ArmorStand hologram7 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram7.setVisible(false);
            hologram7.setCustomNameVisible(true);
            hologram7.setGravity(false);
            hologram7.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(5)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(5)));
            if(!(all_armorstands.contains(hologram7))){
                all_armorstands.add(hologram7);
            }

            ArmorStand hologram8 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram8.setVisible(false);
            hologram8.setCustomNameVisible(true);
            hologram8.setGravity(false);
            hologram8.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(6)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(6)));
            if(!(all_armorstands.contains(hologram8))){
                all_armorstands.add(hologram8);
            }
        }else if (PlayerLib.players.size() == 8){

            ArmorStand hologram = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location, EntityType.ARMOR_STAND);
            hologram.setVisible(false);
            hologram.setCustomNameVisible(true);
            hologram.setCustomName(ChatColor.DARK_RED+"Leaderboard");
            hologram.setGravity(false);
            if(!(all_armorstands.contains(hologram))){
                all_armorstands.add(hologram);
            }

            ArmorStand hologram2 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.5,0), EntityType.ARMOR_STAND);
            hologram2.setVisible(false);
            hologram2.setCustomNameVisible(true);
            hologram2.setGravity(false);
            hologram2.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(0)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(0)));
            if(!(all_armorstands.contains(hologram2))){
                all_armorstands.add(hologram2);
            }

            ArmorStand hologram3 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram3.setVisible(false);
            hologram3.setCustomNameVisible(true);
            hologram3.setGravity(false);
            hologram3.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(1)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(1)));
            if(!(all_armorstands.contains(hologram3))){
                all_armorstands.add(hologram3);
            }

            ArmorStand hologram4 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram4.setVisible(false);
            hologram4.setCustomNameVisible(true);
            hologram4.setGravity(false);
            hologram4.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(2)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(2)));
            if(!(all_armorstands.contains(hologram4))){
                all_armorstands.add(hologram4);
            }

            ArmorStand hologram5 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram5.setVisible(false);
            hologram5.setCustomNameVisible(true);
            hologram5.setGravity(false);
            hologram5.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(3)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(3)));
            if(!(all_armorstands.contains(hologram5))){
                all_armorstands.add(hologram5);
            }

            ArmorStand hologram6 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram6.setVisible(false);
            hologram6.setCustomNameVisible(true);
            hologram6.setGravity(false);
            hologram6.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(4)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(4)));
            if(!(all_armorstands.contains(hologram6))){
                all_armorstands.add(hologram6);
            }

            ArmorStand hologram7 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram7.setVisible(false);
            hologram7.setCustomNameVisible(true);
            hologram7.setGravity(false);
            hologram7.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(5)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(5)));
            if(!(all_armorstands.contains(hologram7))){
                all_armorstands.add(hologram7);
            }

            ArmorStand hologram8 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram8.setVisible(false);
            hologram8.setCustomNameVisible(true);
            hologram8.setGravity(false);
            hologram8.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(6)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(6)));
            if(!(all_armorstands.contains(hologram8))){
                all_armorstands.add(hologram8);
            }

            ArmorStand hologram9 = (ArmorStand) leaderboard_location.getWorld().spawnEntity(leaderboard_location.add(0,-0.3,0), EntityType.ARMOR_STAND);
            hologram9.setVisible(false);
            hologram9.setCustomNameVisible(true);
            hologram9.setGravity(false);
            hologram9.setCustomName(ChatColor.AQUA+ Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(7)).getName()+" "+ChatColor.RED+LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(7)));
            if(!(all_armorstands.contains(hologram9))){
                all_armorstands.add(hologram9);
            }
        }
    }

    public static void delete_armorstands (){
        if(!all_armorstands.isEmpty()) {
            for (ArmorStand armorstand : all_armorstands) {
                armorstand.remove();
            }
        }
    }

    public static void update_armorstands(){
        delete_armorstands();
        spawn();
    }


}
