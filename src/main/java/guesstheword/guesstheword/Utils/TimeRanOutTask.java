package guesstheword.guesstheword.Utils;

import guesstheword.guesstheword.GuessTheWord;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

public class TimeRanOutTask extends BukkitRunnable {
    GuessTheWord plugin;

    public TimeRanOutTask(GuessTheWord plugin) {
        this.plugin = plugin;
    }

    @Override
    public void run() {
        for(UUID uuid:PlayerLib.players){
            Bukkit.getPlayer(uuid).sendMessage(ChatColor.DARK_BLUE+"Time has ran out!");
        }

    }
}
