package guesstheword.guesstheword.Utils;

import org.bukkit.inventory.ItemStack;

import javax.xml.stream.Location;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.UUID;

public class ReturnLibrary{
    public static Hashtable<UUID, ArrayList<Double>> xyz_hashtable = new Hashtable<>();
    public static Hashtable<UUID, String> world_hashtable = new Hashtable<>();
    public static Hashtable<UUID, ItemStack[]> inventory_hashtable = new Hashtable<>();


    public static void save_return(UUID uuid, ArrayList<Double> xyz_list, ItemStack[] inventory, String world){
        if(!(xyz_hashtable.containsKey(uuid))){
            xyz_hashtable.put(uuid, xyz_list);
            world_hashtable.put(uuid, world);
            inventory_hashtable.put(uuid,inventory);
        }else{
            xyz_hashtable.replace(uuid, xyz_list);
            world_hashtable.replace(uuid, world);
            inventory_hashtable.replace(uuid,inventory);
        }
    }
}
