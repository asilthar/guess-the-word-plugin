package guesstheword.guesstheword.Utils;

import guesstheword.guesstheword.GuessTheWord;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.UUID;

public class PlayerOutTask extends BukkitRunnable {

    GuessTheWord plugin;
    Player p;

    public PlayerOutTask(GuessTheWord plugin, Player p) {
        this.plugin = plugin;
        this.p = p;
    }

    @Override
    public void run() {

        //Declare round no longer in progress
        GameRound.is_in_progress=false;

        //Remove the blocks the builder placed
        for(ArrayList<Integer> xyz:PlayerLib.placed_block_locations){
            Location block_location = new Location(Bukkit.getWorld(plugin.getConfig().getString("lobby-location.world")),
                    xyz.get(0),xyz.get(1),xyz.get(2));
            block_location.getBlock().setType(Material.AIR);
        }

        //Player is removed from the builder list
        PlayerLib.removeBuilder(p.getUniqueId());

        //inventory is cleared but he can keep golden helmet if he has it
        if(p.getInventory().contains(new ItemStack(Material.GOLDEN_HELMET))){
            p.getInventory().clear();
            p.getInventory().setHelmet(new ItemStack(Material.GOLDEN_HELMET));

        }else{
            p.getInventory().clear();
        }

        //players who answered in the last round are reset
        PlayerLib.reset_correct_answer();

        //The builder shouldn't be in the players_not_yet_played list anymore
        PlayerLib.players_not_yet_played.remove(p.getUniqueId());
        p.setGameMode(GameMode.SURVIVAL);
        p.setAllowFlight(true);

        //get lobby location from config and teleport him there
        Location lobby = new Location(Bukkit.getWorld(plugin.getConfig().getString("lobby-location.world")),
                plugin.getConfig().getDouble("lobby-location.x"),
                plugin.getConfig().getDouble("lobby-location.y"),
                plugin.getConfig().getDouble("lobby-location.z"));
        p.teleport(lobby);

        //check if he was the last player for the round
        if(!PlayerLib.players_not_yet_played.isEmpty()){

            //not all players have played yet
            //Enter the next builder
            BukkitTask task = new PlayerInTask(plugin,Bukkit.getPlayer(PlayerLib.players_not_yet_played.get(0))).runTask(plugin);
        }else{

            //All players have played
            //the round has ended
            //check if it was the final set
            if(GameRound.current_set==GameRound.total_sets){

                //reassign the lobby scoreboard to everyone
                for(UUID uuid:PlayerLib.players) {
                    Scoreboards.lobbyScoreboard(Bukkit.getPlayer(uuid));
                }

                //check it was a draw
                if(LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(0))==LeaderBoardLib.score.get(LeaderBoardLib.leaderboard_sorted.get(1))){

                    //if its a draw announce draw
                    for(UUID uuid:PlayerLib.players){
                        Bukkit.getPlayer(uuid).sendMessage(ChatColor.AQUA+"The game has ended!");
                        Bukkit.getPlayer(uuid).sendMessage(ChatColor.GREEN+"Its a"+ChatColor.LIGHT_PURPLE+" draw"+ChatColor.GOLD+"!");
                    }
                }else{

                    //if there is a winner announce the winner to everyone
                    for(UUID uuid:PlayerLib.players){
                        Bukkit.getPlayer(uuid).sendMessage(ChatColor.AQUA+"The game has ended!");
                        Bukkit.getPlayer(uuid).sendMessage(ChatColor.GREEN+"The winner is "+ChatColor.GOLD+""+ChatColor.BOLD+Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(0)).getName()+ChatColor.GREEN+"!");
                        Bukkit.getPlayer(uuid).sendTitle(ChatColor.GREEN+"The winner is "+ChatColor.GOLD+""+ChatColor.BOLD+Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(0)).getName()+ChatColor.GREEN+"!",
                                ChatColor.GREEN+"He is awarded the"+ChatColor.GOLD+" Golden Helmet "+ChatColor.GREEN+"of awesomeness",5,200,5);
                    }

                    //clear everyone's inventory to remove the old golden helmet
                    for(UUID uuid:PlayerLib.players){
                        Bukkit.getPlayer(uuid).getInventory().clear();
                    }

                    //give the winner his golden helmet
                    Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(0)).getInventory().setHelmet(new ItemStack(Material.GOLDEN_HELMET));
//                    Bukkit.getPlayer(LeaderBoardLib.leaderboard_sorted.get(0)).sendTitle(ChatColor.GREEN+"You have been awarded the"+ChatColor.GOLD+"Golden Helmet",
//                            "You can keep it until someone else wins",5,40,5);

                }

            //it was not-> start the next set
            }else{


                //set current_set to the next one
                GameRound.current_set++;

                //announce that the round has ended
                for(UUID uuid:PlayerLib.players){
                    Bukkit.getPlayer(uuid).sendTitle(ChatColor.AQUA+"Round "+ChatColor.GOLD+GameRound.current_set+ChatColor.AQUA+" has ended!",
                            ChatColor.GREEN+"Starting new round...",5,50,5);
                }

                //reset the players_not_yet_played
                PlayerLib.resetNotPlayedPlayers();

                //start looping through not_played_players again
                BukkitTask task = new PlayerInTask(plugin,Bukkit.getPlayer(PlayerLib.players_not_yet_played.get(0))).runTaskLater(plugin,60);

            }
        }
    }
}
