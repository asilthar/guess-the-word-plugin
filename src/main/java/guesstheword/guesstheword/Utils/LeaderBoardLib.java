package guesstheword.guesstheword.Utils;

import org.bukkit.Bukkit;

import java.util.*;

public class LeaderBoardLib {
    public static HashMap<UUID, Integer> score = new HashMap<>();
    public static ArrayList<UUID> leaderboard_sorted = new ArrayList<>();


    public static void sort_score(){
        leaderboard_sorted = new ArrayList<>();
        HashMap<UUID, Integer> score_temp = (HashMap<UUID, Integer>) score.clone();
        for (UUID uuid : score.keySet()){
            if (leaderboard_sorted.size()==0){
                leaderboard_sorted.add(uuid);
            }else{
                int size = leaderboard_sorted.size();
                for(int i=0;i<size;i++){
                    if(score_temp.get(leaderboard_sorted.get(i))<=score_temp.get(uuid)){
                        leaderboard_sorted.add(i,uuid);
                        break;
                    }else if(i==leaderboard_sorted.size()-1){
                        leaderboard_sorted.add(uuid);
                        break;
                    }
                }
            }
        }

    }





    public static void add_point(UUID uuid, Integer points){
        Integer new_score = score.get(uuid)+points;
        score.replace(uuid,new_score);
    }

    public static void reset_score(){
        score = new HashMap<>();
        for(UUID uuid:PlayerLib.players){
            score.put(uuid,0);
        }
    }


}
