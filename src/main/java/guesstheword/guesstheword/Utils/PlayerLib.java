package guesstheword.guesstheword.Utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.UUID;

public class PlayerLib {
    public static ArrayList<UUID> players = new ArrayList<>();
    public static ArrayList<UUID> players_not_yet_played;
    public static ArrayList<UUID> builder = new ArrayList<>();
    public static ArrayList<ArrayList<Integer>> placed_block_locations = new ArrayList<>();
    public static ArrayList<UUID> correct_answer = new ArrayList<>();
    public static ArrayList<UUID> has_answered = new ArrayList<>();



    public static void resetNotPlayedPlayers (){
        players_not_yet_played = (ArrayList<UUID>) players.clone();
    }

    public static void addPlayer(Player p){
        if(players.contains(p.getUniqueId())){
            p.sendMessage(ChatColor.RED+"You are already an active player");
        }else{
            players.add(p.getUniqueId());
            p.sendMessage(ChatColor.GREEN+"Joined WordGuess");
        }
    }

    public static void removePlayer(Player p) {
        if (!players.contains(p.getUniqueId())) {
            p.sendMessage(ChatColor.RED+"You are not an active player.");

        }else{
            players.remove(p.getUniqueId());
            p.sendMessage(ChatColor.GREEN+"You have left WordGuess");
        }
    }

    public static void addBuilder(UUID uuid){
        builder.add(uuid);
    }

    public static void removeBuilder(UUID uuid){
        builder.remove(uuid);
    }
    public static void addBlock(Block block){
        ArrayList<Integer> xyz = new ArrayList<>();
        int x = block.getX();
        int y = block.getY();
        int z = block.getZ();
        xyz.add(x);
        xyz.add(y);
        xyz.add(z);
        placed_block_locations.add(xyz);
    }
    public static void removeBlock(Block block){
        ArrayList<Integer> xyz = new ArrayList<>();
        int x = block.getX();
        int y = block.getY();
        int z = block.getZ();
        xyz.add(x);
        xyz.add(y);
        xyz.add(z);
        placed_block_locations.remove(xyz);
    }

    public static Boolean is_placed_block(Block block){
        ArrayList<Integer> placed_block_xyz = new ArrayList<>();
        int x = block.getX();
        int y = block.getY();
        int z = block.getZ();
        placed_block_xyz.add(x);
        placed_block_xyz.add(y);
        placed_block_xyz.add(z);
        for(ArrayList<Integer> xyz:placed_block_locations){
            if(xyz.equals(placed_block_xyz)){
                return true;
            }
        }
        return false;


    }

    public static void add_correct_answer(UUID uuid){
        correct_answer.add(uuid);
    }

    public static void reset_correct_answer(){
        correct_answer = new ArrayList<>();
    }

    public static void add_has_answered(UUID uuid){
        has_answered.add(uuid);
    }

    public  static void reset_has_answered(){
        has_answered = new ArrayList<>();
    }

    public static void force_quit_all(){
        for(UUID uuid:players){
            Player p = Bukkit.getPlayer(uuid);
            removePlayer(p);
            p.setAllowFlight(false);
            Scoreboards.clearScoreboard(p);
            LeaderboardHologram.update_armorstands();
            ArrayList<Double> return_xyz = ReturnLibrary.xyz_hashtable.get(p.getUniqueId());
            double x = return_xyz.get(0);
            double y = return_xyz.get(1);
            double z = return_xyz.get(2);
            String return_world_string = ReturnLibrary.world_hashtable.get(p.getUniqueId());
            World return_world = Bukkit.getWorld(return_world_string);
            Location return_location = new Location(return_world, x, y, z);
            p.teleport(return_location);
            ItemStack[] old_inv = ReturnLibrary.inventory_hashtable.get(p.getUniqueId());
            p.getInventory().clear();
            for (int i = 0; i < old_inv.length; i++) {
                ItemStack item = old_inv[i];
                if (!(item == null)) {
                    p.getInventory().setItem(i, item);
                }
            }

        }


    }

}
