package guesstheword.guesstheword.Utils;

import guesstheword.guesstheword.GuessTheWord;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

public class PlayerInTask extends BukkitRunnable {
    GuessTheWord plugin;
    Player p;

    public PlayerInTask(GuessTheWord plugin, Player p) {
        this.plugin = plugin;
        this.p = p;
    }

    @Override
    public void run() {
        //declare round in progress
        GameRound.is_in_progress=true;

        //teleport the builder to the building spot
        Location in_location = new Location(Bukkit.getWorld(plugin.getConfig().getString("creation-room.world")),
                plugin.getConfig().getDouble("creation-room.x"),
                plugin.getConfig().getDouble("creation-room.y"),
                plugin.getConfig().getDouble("creation-room.z"));
        p.teleport(in_location);

        //declare him a builder so that he is able to place blocks. His blocks are registered by BlockBreakHandler and deleted after.
        //he cant break blocks that he hasn't placed(like the floor)
        PlayerLib.builder.add(p.getUniqueId());
        p.setGameMode(GameMode.CREATIVE);

        //get a new building word
        Drawings.setNewDrawing();
        PlayerLib.reset_has_answered();
        PlayerLib.reset_correct_answer();

        //send it to the builder
        p.sendMessage(ChatColor.AQUA+"Your word is: "+ChatColor.GOLD+""+ChatColor.BOLD+Drawings.current_drawing);
        p.sendTitle(ChatColor.AQUA+"Your word is: "+ChatColor.GOLD+""+ChatColor.BOLD+Drawings.current_drawing,"",5,40,5);


        //remove the builder when the round time has ended
        Integer ticks_per_round = plugin.getConfig().getInt("minutes-per-round")*60*20;
        BukkitTask time = new TimeRanOutTask(plugin).runTaskLater(plugin,ticks_per_round);
        BukkitTask task = new PlayerOutTask(plugin,Bukkit.getPlayer(PlayerLib.players_not_yet_played.get(0))).runTaskLater(plugin,ticks_per_round);

        //start the scoreboard time counter
        BukkitTask scoreboard_timer = new ScoreboardTimeTask(plugin,plugin.getConfig().getInt("minutes-per-round")*60).runTaskTimer(plugin,0,20);

    }
}
