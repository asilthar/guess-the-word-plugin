package guesstheword.guesstheword.Commands;

import guesstheword.guesstheword.Utils.PlayerLib;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class GuessTheWordCommandTAB implements TabCompleter {

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        List<String> arguments = new ArrayList<>();
        Player p = (Player)sender;
        if(p.hasPermission("guesstheword.admin")){
            arguments.add("join");
            arguments.add("quit");
            arguments.add("start");
            arguments.add("setleaderboard");
            arguments.add("spawnleaderboard");
            arguments.add("setlobby");
            arguments.add("setcreationroom");
            arguments.add("reload");
            arguments.add("clearholos");
            arguments.add("setroundminutes");
            arguments.add("settotalsets");
            arguments.add("quitall");


        }else{
            arguments.add("join");
            arguments.add("quit");
            arguments.add("start");
        }



        return arguments;
    }
}
