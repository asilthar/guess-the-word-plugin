package guesstheword.guesstheword.Commands;

import guesstheword.guesstheword.GuessTheWord;
import guesstheword.guesstheword.Utils.*;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scheduler.BukkitWorker;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class GuessTheWordCommand implements CommandExecutor {
    GuessTheWord plugin;

    public GuessTheWordCommand(GuessTheWord plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = ((Player) sender).getPlayer();
            World world_guessword = Bukkit.getWorld(plugin.getConfig().getString("lobby-location.world"));
            if (args.length == 0) {
                p.sendMessage(ChatColor.GREEN + "Usage:");

            } else if (args.length == 1 && args[0].equalsIgnoreCase("join")) {
                Scoreboards.lobbyScoreboard(p);

                if (!(PlayerLib.players.contains(p.getUniqueId()))) {
                    double x = plugin.getConfig().getDouble("lobby-location.x");
                    double y = plugin.getConfig().getDouble("lobby-location.y");
                    double z = plugin.getConfig().getDouble("lobby-location.z");

                    Location lobby = new Location(world_guessword, x, y, z);
                    ArrayList<Double> xyz_list = new ArrayList<>();
                    xyz_list.add((double) p.getLocation().getBlockX());
                    xyz_list.add((double) p.getLocation().getBlockY());
                    xyz_list.add((double) p.getLocation().getBlockZ());
                    ReturnLibrary.save_return(p.getUniqueId(), xyz_list, p.getInventory().getContents(), p.getWorld().getName());
                    p.teleport(lobby);
                    PlayerLib.addPlayer(p);
                    LeaderBoardLib.reset_score();
                    LeaderBoardLib.sort_score();
                    LeaderboardHologram.update_armorstands();
                    p.setAllowFlight(true);
                    p.getInventory().clear();
                } else {
                    p.sendMessage(ChatColor.RED + "You have already joined the game");
                }




            } else if (args.length == 1 && args[0].equalsIgnoreCase("quit")) {

                if(PlayerLib.players.contains(p.getUniqueId())){
                    PlayerLib.removePlayer(p);
                    LeaderboardHologram.spawn();
                    if (!(p.getLocation().getWorld().equals(world_guessword))) {
                        p.sendMessage(ChatColor.RED + "You are not in " + plugin.getConfig().getString("lobby-location.world"));
                        p.sendMessage("You are in " + p.getWorld().toString());
                    } else if (!(ReturnLibrary.xyz_hashtable.containsKey(p.getUniqueId()))) {
                        p.sendMessage(ChatColor.RED + "No saved data");
                    } else {
                        p.setAllowFlight(false);
                        Scoreboards.clearScoreboard(p);
                        LeaderboardHologram.update_armorstands();
                        ArrayList<Double> return_xyz = ReturnLibrary.xyz_hashtable.get(p.getUniqueId());
                        double x = return_xyz.get(0);
                        double y = return_xyz.get(1);
                        double z = return_xyz.get(2);
                        String return_world_string = ReturnLibrary.world_hashtable.get(p.getUniqueId());
                        World return_world = Bukkit.getWorld(return_world_string);
                        Location return_location = new Location(return_world, x, y, z);
                        p.teleport(return_location);
                        ItemStack[] old_inv = ReturnLibrary.inventory_hashtable.get(p.getUniqueId());
                        p.getInventory().clear();
                        for (int i = 0; i < old_inv.length; i++) {
                            ItemStack item = old_inv[i];
                            if (!(item == null)) {
                                p.getInventory().setItem(i, item);
                            }
                        }

                    }
                }else{
                    p.sendMessage(ChatColor.RED+"You have not joined GuesstheWord");
                }

            } else if (args.length == 1 && args[0].equalsIgnoreCase("setlobby")) {
                if (p.hasPermission("guesstheword.admin")) {
                    p.sendMessage(ChatColor.AQUA+"Setting new lobby");
                    Location lobby = p.getLocation();
                    plugin.getConfig().set("lobby-location.x", lobby.getBlockX());
                    plugin.getConfig().set("lobby-location.y", lobby.getBlockY());
                    plugin.getConfig().set("lobby-location.z", lobby.getBlockZ());
                    plugin.getConfig().set("lobby-location.world", lobby.getWorld().getName());
                    plugin.saveConfig();
                    plugin.reloadConfig();
                }

            } else if (args.length == 1 && args[0].equalsIgnoreCase("setcreationroom")) {
                if (p.hasPermission("guesstheword.admin")) {
                    p.sendMessage(ChatColor.AQUA+"Setting new creation room");
                    Location lobby = p.getLocation();
                    plugin.getConfig().set("creation-room.x", lobby.getBlockX());
                    plugin.getConfig().set("creation-room.y", lobby.getBlockY());
                    plugin.getConfig().set("creation-room.z", lobby.getBlockZ());
                    plugin.getConfig().set("creation-room.world", lobby.getWorld().getName());
                    plugin.saveConfig();
                    plugin.reloadConfig();
                }

            }else if (args.length == 1 && args[0].equalsIgnoreCase("spawnleaderboard")){
                LeaderboardHologram.spawn();

            }else if (args.length == 1 && args[0].equalsIgnoreCase("setleaderboard")){
                if (p.hasPermission("guesstheword.admin")){
                    p.sendMessage(ChatColor.AQUA+"Setting new leaderboard");
                    Location loc = p.getLocation();
                    LeaderboardHologram.delete_armorstands();
                    plugin.getConfig().set("leaderboard-location.world",p.getWorld().getName());
                    plugin.getConfig().set("leaderboard-location.x",p.getLocation().getX());
                    plugin.getConfig().set("leaderboard-location.y",(p.getLocation().getY())+2.0);
                    plugin.getConfig().set("leaderboard-location.z",p.getLocation().getZ());
                    plugin.saveConfig();
                    plugin.reloadConfig();
                }



            }else if (args.length == 1 && args[0].equalsIgnoreCase("reload")){
                plugin.reloadConfig();

            }else if (args.length == 1 && args[0].equalsIgnoreCase("clearholos")){
                LeaderboardHologram.delete_armorstands();

            }else if (args.length == 1 && args[0].equalsIgnoreCase("start")){
                if(PlayerLib.players.size()>2){
                    GameRound.current_set = 1;
                    LeaderBoardLib.reset_score();
                    PlayerLib.resetNotPlayedPlayers();
                    int ticks_per_round = plugin.getConfig().getInt("minutes-per-round")*60*20;
                    BukkitTask start = new PlayerInTask(plugin,Bukkit.getPlayer(PlayerLib.players_not_yet_played.get(0))).runTask(plugin);
                }else{
                    p.sendMessage(ChatColor.RED+"You need at least 3 players to start the game");
                }


            }else if (args.length == 1 && args[0].equalsIgnoreCase("setroundminutes")){
                p.sendMessage(ChatColor.GREEN+"Usage: /worguess setroundminutes <minutes>");

            }else if (args.length == 2 && args[0].equalsIgnoreCase("setroundminutes")){
                try{
                    p.sendMessage(ChatColor.GREEN+"Setting round minutes to "+ChatColor.GOLD+args[1]);
                    Integer minutes = Integer.valueOf(args[1]);
                    plugin.getConfig().set("minutes-per-round",minutes);
                    plugin.saveConfig();
                    plugin.reloadConfig();
                    for(UUID uuid:PlayerLib.players){
                        Scoreboards.lobbyScoreboard(Bukkit.getPlayer(uuid));
                    }
                }catch(NumberFormatException ex){
                    p.sendMessage("Usage: /worguess setroundminutes <minutes>");
                }
            }else if (args.length == 1 && args[0].equalsIgnoreCase("reset")){
                if (p.hasPermission("guesstheword.admin")){

                    //this command should reset the current game
                    //first all the running and pending tasks are gathered
                    BukkitScheduler scheduler = plugin.getServer().getScheduler();
                    List<BukkitWorker> running_tasks = scheduler.getActiveWorkers();
                    List<BukkitTask> pending_tasks = scheduler.getPendingTasks();


                    //check if there is a builder to extract
                    if(!PlayerLib.builder.isEmpty()){

                        //Extract the builder without inserting the next player
                        Player builder = Bukkit.getPlayer(PlayerLib.builder.get(0));


                        //players who answered in the last round are reset
                        PlayerLib.reset_correct_answer();

                        //get lobby location from config and teleport him there
                        Location lobby = new Location(Bukkit.getWorld(plugin.getConfig().getString("lobby-location.world")),
                                plugin.getConfig().getDouble("lobby-location.x"),
                                plugin.getConfig().getDouble("lobby-location.y"),
                                plugin.getConfig().getDouble("lobby-location.z"));
                        builder.teleport(lobby);

                        //Clear his inventory(if he has golden helmet he keeps it)
                        //Player is removed from the builder list
                        if(builder.getInventory().contains(new ItemStack(Material.GOLDEN_HELMET))){
                            builder.getInventory().clear();
                            builder.getInventory().setHelmet(new ItemStack(Material.GOLDEN_HELMET));
                        }else{
                            builder.getInventory().clear();
                        }

                        PlayerLib.removeBuilder(builder.getUniqueId());
                    }

                    //all scheduled tasks are cancelled
                    if(!running_tasks.isEmpty()){
                        for (BukkitWorker task: running_tasks ){
                            int id = task.getTaskId();

                            scheduler.cancelTask(id);
                        }
                    }
                    if(!pending_tasks.isEmpty()){
                        for (BukkitTask task:pending_tasks){
                            int id = task.getTaskId();
                            scheduler.cancelTask(id);
                        }
                    }

                    //reassign the lobby scoreboard
                    for(UUID uuid:PlayerLib.players) {
                        Scoreboards.lobbyScoreboard(Bukkit.getPlayer(uuid));
                    }

                }

            }else if (args.length == 2 && args[0].equalsIgnoreCase("settotalsets")){
                try{
                    p.sendMessage(ChatColor.GREEN+"Setting total sets to "+ChatColor.GOLD+args[1]);
                    Integer sets = Integer.valueOf(args[1]);
                    plugin.getConfig().set("game-sets",sets);
                    plugin.saveConfig();
                    plugin.reloadConfig();
                    GameRound.reload_total_sets();
                    for(UUID uuid:PlayerLib.players){
                        Scoreboards.lobbyScoreboard(Bukkit.getPlayer(uuid));
                    }
                }catch(NumberFormatException ex) {
                    p.sendMessage("Usage: /worguess settotalsets <sets>");
                }
            }else if (args.length == 2 && args[0].equalsIgnoreCase("quitall")){
                PlayerLib.force_quit_all();
            }
        }
        return true;
    }
}