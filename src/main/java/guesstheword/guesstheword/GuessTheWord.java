package guesstheword.guesstheword;

import guesstheword.guesstheword.Commands.GuessTheWordCommand;
import guesstheword.guesstheword.Commands.GuessTheWordCommandTAB;
import guesstheword.guesstheword.Events.BlockBreakHandler;
import guesstheword.guesstheword.Events.BlockPlaceHandler;
import guesstheword.guesstheword.Events.PlayerChatHandler;
import guesstheword.guesstheword.Events.PlayerHitHandler;
import guesstheword.guesstheword.Utils.LeaderBoardLib;
import guesstheword.guesstheword.Utils.LeaderboardHologram;
import guesstheword.guesstheword.Utils.PlayerLib;
import guesstheword.guesstheword.Utils.Scoreboards;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.UUID;

public final class GuessTheWord extends JavaPlugin {

    //gives static instance
    private static JavaPlugin plugin;
    public static JavaPlugin getInst() {
        return plugin;
    }

    @Override
    public void onEnable() {

        // Plugin startup logic
        plugin = this;
        getConfig().options().copyDefaults();
        saveDefaultConfig();
        getCommand("wordguess").setExecutor(new GuessTheWordCommand(this));
        getCommand("wordguess").setTabCompleter(new GuessTheWordCommandTAB());
        getServer().getPluginManager().registerEvents(new BlockBreakHandler(),this);
        getServer().getPluginManager().registerEvents(new BlockPlaceHandler(),this);
        getServer().getPluginManager().registerEvents(new PlayerChatHandler(this),this);
        getServer().getPluginManager().registerEvents(new PlayerHitHandler(),this);

    }

    @Override
    public void onDisable() {
       LeaderboardHologram.delete_armorstands();
       for(UUID uuid: PlayerLib.players){
           Scoreboards.clearScoreboard(Bukkit.getPlayer(uuid));
       }
       PlayerLib.force_quit_all();
    }
}
