package guesstheword.guesstheword.Events;

import guesstheword.guesstheword.Utils.PlayerLib;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class PlayerHitHandler implements Listener {

    @EventHandler
    public void onPlayerHit(EntityDamageByEntityEvent e){
        if (e.getEntity() instanceof Player){
            Player p = (Player) e.getEntity();
            if(!PlayerLib.players.isEmpty())
            {
                if (PlayerLib.players.contains(p)) {
                    e.setCancelled(true);
                }
            }
        }
    }
}
