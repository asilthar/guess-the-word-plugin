package guesstheword.guesstheword.Events;

import guesstheword.guesstheword.GuessTheWord;
import guesstheword.guesstheword.Utils.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

public class PlayerChatHandler implements Listener {
    GuessTheWord plugin;

    public PlayerChatHandler(GuessTheWord plugin) {
        this.plugin = plugin;
    }


    @EventHandler
    public void onPlayerChat(PlayerChatEvent e){
        Player p = e.getPlayer();

        //Non-players and builders should not be able to answer. Same for people who already answered
        if(!PlayerLib.players.isEmpty())
        {
            if (PlayerLib.players.contains(p.getUniqueId())
                    && !(PlayerLib.builder.contains(p.getUniqueId()))
                    && !(PlayerLib.has_answered.contains(p.getUniqueId())
                    && (GameRound.is_in_progress))) {

                //get the answer
                String answer = e.getMessage();

                //check if it was correct
                String drawing = Drawings.getCurrent_drawing();


                if (answer.equalsIgnoreCase(drawing)) {

                    //get how much time has passed
                    Integer current_seconds = ScoreboardTimeTask.counter;

                    //get total round time
                    Integer total_seconds = plugin.getConfig().getInt("minutes-per-round") * 60;


                    //cancel his message so other players don't see it
                    e.setCancelled(true);

                    //tell him he got the answer correct
                    p.sendTitle(ChatColor.GREEN + "" + ChatColor.BOLD + "Correct!", "", 5, 40, 5);

                    //add him to the list of people who found the answer
                    PlayerLib.add_correct_answer(p.getUniqueId());

                    //make sure he can't answer again
                    PlayerLib.add_has_answered(p.getUniqueId());

                    //Give players better awards depending on how fast they found the answer with a lowest limit of 50 points
                    Integer award = 0;
                    if (200 / PlayerLib.has_answered.size() >= 50) {
                        award = 200 / PlayerLib.has_answered.size();
                    } else {
                        award = 50;
                    }
                    LeaderBoardLib.add_point(p.getUniqueId(), award);

                    //the builder gets 100 points
                    LeaderBoardLib.add_point(PlayerLib.builder.get(0), 100);


                    //Sort the new score and update the leaderboard
                    LeaderBoardLib.sort_score();
                    LeaderboardHologram.update_armorstands();
                    p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);


                    //tell everyone he has found the answer
                    for (UUID uuid : PlayerLib.players) {
                        Bukkit.getPlayer(uuid).sendMessage(ChatColor.AQUA + p.getName() + " has found the word!");
                        Bukkit.getPlayer(uuid).sendMessage(ChatColor.GREEN + p.getName() + " was awarder " + ChatColor.GOLD + award + ChatColor.GREEN + " points");
                    }


                    //check if everyone has found the answer to end the round if that's the case
                    if (PlayerLib.correct_answer.size() == PlayerLib.players.size() - 1) {
                        for (UUID uuid : PlayerLib.players) {
                            Bukkit.getPlayer(uuid).sendMessage(ChatColor.DARK_PURPLE + "Everyone found the word!");
                            PlayerLib.reset_correct_answer();
                        }
                        GameRound.skipRound();
                    }

                } else {

                    //tell him his answer was wrong
                    p.sendTitle(ChatColor.RED + "" + ChatColor.BOLD + "Wrong!", "", 5, 40, 5);
                }

            }
        }


    }
}
