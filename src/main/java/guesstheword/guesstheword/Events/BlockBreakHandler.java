package guesstheword.guesstheword.Events;

import guesstheword.guesstheword.Utils.PlayerLib;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockBreakHandler implements Listener {

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e){
        Player p = e.getPlayer();
        if(!PlayerLib.players.isEmpty())
        {
            if (PlayerLib.players.contains(p.getUniqueId()) && !p.hasPermission("guesstheword.builder")) {
                if (!(PlayerLib.builder.contains(p.getUniqueId()))) {
                    e.setCancelled(true);
                } else {
                    Block block = e.getBlock();
                    if (!(PlayerLib.is_placed_block(block))) {
                        e.setCancelled(true);
                    } else {
                        PlayerLib.removeBlock(block);
                    }

                }
            }
        }

    }
}
