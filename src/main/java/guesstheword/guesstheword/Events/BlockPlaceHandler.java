package guesstheword.guesstheword.Events;

import guesstheword.guesstheword.Utils.PlayerLib;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockPlaceHandler implements Listener {
    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e){
        Player p = e.getPlayer();
        if(!PlayerLib.builder.isEmpty()) {
            if ((PlayerLib.builder.contains(p.getUniqueId()))) {
                Block placed_block = e.getBlock();
                PlayerLib.addBlock(placed_block);
            }
        }
    }

}
